Flask==1.0.2
Flask-Cors==3.0.7
numpy==1.15.4
pandas==0.23.4
scikit-learn==0.20.3
scipy==1.1.0
