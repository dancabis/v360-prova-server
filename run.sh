# Runs the server in production.

export PYTHONPATH=$PYTHONPATH:/var/v360/v360-prova-ml/src
export V360_CLASSIFIER=/var/v360/ml/classifier.pkl
cd src/v360_server
gunicorn3 -D -b 0.0.0.0:8000 wsgi:app
