'''
Implements a REST API for products. This is the simplest way of doing this, but
it is probably not suitable for bigger applications.

Before running, it is necessary to export the V360_CLASSIFIER environment
variable pointing to the trained classifier file.

It is also necessary to export the V360_PREPROCESS_M environment variable
pointing to the location of the module that has the preprocessing functions
needed by the classifier.
'''

from flask import Flask, request, jsonify
from flask_cors import CORS
import numpy as np
import pickle
import os

# Loads the classifier.
classifier = None
with open(os.environ['V360_CLASSIFIER'], 'rb') as f:
    classifier = pickle.load(f)

# Defines the product names indexed by EAN.
# @TODO Read from database.
names = {
    7896256600223: 'LEITE UHT INTEGRAL TIROL TP 1000ML',
    7896185312396: 'LEITE UHT INTEGRAL SHEFA GARRAFA 1000ML',
    7896185312402: 'LEITE UHT SEMI DESNATADO SHEFA GARRAFA 1000ML',
    7896185312419: 'LEITE UHT DESNATADO SHEFA GARRAFA 1000ML',
    7896256600230: 'LEITE UHT SEMI DESNATADO TIROL TP 1000ML',
    7896256600247: 'LEITE UHT DESNATADO LIGHT TIROL TP 1000ML',
    7896569400244: 'LEITE UHT INTEGRAL LIDER TP CAIXA 12 X 1000ML',
    7896569400206: 'LEITE UHT INTEGRAL LIDER TP 1000ML',
    7896256600339: 'LEITE UHT INTEGRAL TIROL TP CAIXA 12 X 1000ML',
    7896185310033: 'LEITE UHT INTEGRAL SHEFA TP 1000ML',
    7896569400268: 'LEITE UHT DESNATADO LIDER TP CAIXA 12 X 1000ML',
    7896569400251: 'LEITE UHT SEMI DESNATADO LIDER TP CAIXA 12 X 1000ML',
    7896569400220: 'LEITE UHT DESNATADO LIDER TP 1000ML',
    7896185310057: 'LEITE UHT SEMI DESNATADO SHEFA TP 1000ML',
    7896185310040: 'LEITE UHT DESNATADO SHEFA TP 1000ML',
    7896569400213: 'LEITE UHT SEMI DESNATADO LIDER TP 1000ML',
    7896185312433: 'LEITE UHT ZERO LACTOSE SEMI DESNATADO SHEFA GARRAFA 1000ML',
    7896256600315: 'LEITE UHT SEMI DESNATADO TIROL TP 1000ML',
    7896256600322: 'LEITE UHT DESNATADO TIROL TP CAIXA 12 X 1000ML'
}

# Sets up the Flask application.
app = Flask(__name__)

# @TODO: In a real situation, the list of allowed origins should be provided.
CORS(app)

@app.route('/products', methods=['GET'])
def show_products():
    # @TODO Should display a list of products when no description is provided.
    description = request.args['description']

    # Performs the classification.
    probabilities = classifier.predict_proba([description])
    best_idx = np.argmax(probabilities[0])
    ean = classifier.classes_[best_idx]

    # Returns a JSON.
    return jsonify({
        'ean': int(ean),
        'name': names[ean],
        'probability': probabilities[0][best_idx]
    })
